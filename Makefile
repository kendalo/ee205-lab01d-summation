# Build a Summation C program

CC     = gcc
CFLAGS = -g -Wall

TARGET = summation

all: $(TARGET)

hello: summation.c
	$(CC) $(CFLAGS) -o $(TARGET) summation.c

clean:
	rm $(TARGET)

